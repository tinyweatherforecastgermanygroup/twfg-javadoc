"""
polishing the javadoc html output

* add sitemap
* add robots.txt
* html error 404 page
* minify html code
* add meta tags to html head

"""

import concurrent.futures
import html
import logging
import re
from datetime import datetime
from pathlib import Path

from bs4 import BeautifulSoup
from dateutil.tz import (
    tzutc,
)

# TODO: replace bs4 using selectolax

working_dir = Path("../public")
working_dir.mkdir(parents=True, exist_ok=True)  # create directory if not exists

log_p = working_dir / "debug.log"
try:
    logging.basicConfig(
        format="%(asctime)-s %(levelname)s [%(name)s]: %(message)s",
        level=logging.DEBUG,
        handlers=[
            logging.FileHandler(
                str(log_p.absolute()), encoding="utf-8"
            ),
            logging.StreamHandler(),
        ],
    )
except Exception as error_msg:
    logging.error(f"while logger init! -> error: {error_msg}")


def cleanup_html_file(file_path: Path):
    file_path_str = str(file_path.absolute())
    with open(file_path_str, "r", encoding="utf-8") as html_file_old:
        html_file_str = str(html_file_old.read()).strip()

    len_html_str = len(html_file_str)
    if len_html_str < 30:
        logging.warning(f"length ({len_html_str}) of html contents of '{html_file}' is smaller than expected")
        print(html_file_str)

    try:
        html_file_soup = BeautifulSoup(html_file_str, features="html.parser")
    except Exception as error_msg:
        logging.error(f"while parsing html contents of '{file_path}' ({file_path.stat().st_size}) -> error: {error_msg}")
        return False

    #region js minification
    js_files = html_file_soup.select('script[src$=".js"]')
    for js_file in js_files:
        js_file_path = working_dir / Path(
            str(js_file.get("src")).strip().replace("../", "")
        )
        if js_file_path.exists() is True:
            js_file_size = int(js_file_path.stat().st_size)
            if js_file_size < 1:
                logging.debug(
                    f"removing '{js_file}' -> file size ({js_file_size}) is invalid!"
                )
                js_file.decompose()
            else:
                if ".min" not in str(js_file_path):
                    js_file["src"] = (
                        str(js_file.get("src")).strip().replace(".js", ".min.js")
                    )
        else:
            logging.debug(
                f"removing '{js_file}' ({js_file_path}) -> failed to open file!"
            )
            js_file.decompose()
    #endregion

    """
    try:
        script_tags = html_file_soup.select('body > script[type="text/javascript"]')
        if len(script_tags) > 0:
            #pprint(script_tags)
            for script_tag in script_tags:
                if str(script_tag.string[0:4]) == '<!--':
                    logging.debug("removing script tag '"+str(re.sub(r'(?im)[\r\n\t ]+','',str(script_tag.string))).strip()+"' -> contains surplus code ")
                    script_tag.decompose()
    except Exception as e:
        logging.error(f"failed to process 'body > script' tags -> error: {e} ")
    """

    #region css minification
    css_files = html_file_soup.select('link[href$=".css"]')

    for css_file in css_files:
        css_file_path = working_dir / Path(
            str(css_file.get("href")).strip().replace("../", "")
        )
        if css_file_path.exists() is True:
            css_file_size = int(css_file_path.stat().st_size)
            if css_file_size < 1:
                logging.debug(
                    f"removing '{css_file}' -> file size ({css_file_size}) is invalid!"
                )
                css_file.decompose()
            else:
                if ".min" not in str(css_file_path):
                    css_file["href"] = (
                        str(css_file.get("href")).strip().replace(".css", ".min.css")
                    )
                try:
                    if "title=" in str(css_file):
                        del css_file["title"]
                except Exception as error_msg:
                    logging.debug(
                        f"removing the 'title' of '{css_file}'  ({css_file_path}) failed!"
                        f" -> error: {error_msg}"
                    )
        else:
            logging.debug(
                f"removing '{css_file}' ({css_file_path}) -> failed to open file!"
            )
            css_file.decompose()
    #endregion

    #region social metadata -> also see https://ogp.me/
    opengraph = f"""
    <meta name="description" content="TinyWeatherForecastGermany is an open source android app using open weather data provided by Deutscher Wetterdienst (DWD).">
    
    <meta property="og:site_name" content="TinyWeatherForecastGermany - Javadoc" />
    <meta property="og:locale" content="en_GB" />
    <meta property="og:title" content="Tiny Weather Forecast Germany - javadoc code documentation">
    <meta property="og:description" content="TinyWeatherForecastGermany is an open source android app using open weather data provided by Deutscher Wetterdienst (DWD).">
    <meta property="og:image" content="https://tinyweatherforecastgermanygroup.gitlab.io/index/images/twfg-repository-open-graph-graphic.png">
    <meta property="og:url" content="https://tinyweatherforecastgermanygroup.gitlab.io/twfg-javadoc/{str(file_path).replace('public/','').replace('../','')}">

    <meta name="twitter:title" content="Tiny Weather Forecast Germany - javadoc code documentation">
    <meta name="twitter:description" content="TinyWeatherForecastGermany is an open source android app using open weather data provided by Deutscher Wetterdienst (DWD).">
    <meta name="twitter:image" content="https://tinyweatherforecastgermanygroup.gitlab.io/index/images/twfg-repository-open-graph-graphic.png">
    <meta name="twitter:card" content="summary_large_image">
    """

    html_file_soup.select("head")[0].append(
        BeautifulSoup(opengraph, features="html.parser")
    )  # append social media metadata to 'head' of the html document

    #endregion

    #region meta -> keywords
    try:
        keyword_tags = html_file_soup.select('meta[name="keywords"]')
        keyword_str = ""
        quotes_pattern = re.compile(r"(?m)[\'\"]+")
        tags_pattern = re.compile(r"(?m)<[^>]+>")
        if len(keyword_tags) > 0:
            for keyword_tag in keyword_tags:
                try:
                    keyword_decoded = str(
                        html.unescape(keyword_tag["content"]).strip().strip(",")
                    )
                    keyword_str += (
                        re.sub(
                            quotes_pattern,
                            "",
                            str(re.sub(tags_pattern, "", keyword_decoded)),
                        )
                        + ","
                    )
                    if "<" in keyword_decoded:
                        logging.debug(keyword_decoded)
                        logging.debug(keyword_str)
                    keyword_tag.decompose()
                except Exception as error_msg:
                    logging.error(
                        f" while processing 'meta -> keywords' tag '{keyword_tag}' of {file_path}"
                        f" -> error: {error_msg}"
                    )
        else:
            keyword_str = "Tiny, Weather, Forecast, Germany, source, code, Deutschland, Wetter, Vorhersage, DWD, Deutscher Wetterdienst"
        html_file_soup.select("head")[0].insert(
            0,
            BeautifulSoup(
                f'<meta name="keywords" content="{str(keyword_str).strip().strip(",")}">',
                features="html.parser",
            ),
        )  # append social media metadata to 'head' of the html document
    except Exception as error_msg:
        logging.error(
            f" while processing 'meta -> keywords' tags of {file_path} -> error: {error_msg}"
        )
    #endregion

    try:
        # looking for -> <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
        meta_charset_tag = html_file_soup.select('meta[http-equiv="Content-Type"]')
        if len(meta_charset_tag) > 0:
            meta_charset_tag[0].decompose()
        html_file_soup.select("head")[0].insert(
            0, BeautifulSoup('<meta charset="utf-8">', features="html.parser")
        )  # append correct document encoding/charset metadata tag
    except Exception as error_msg:
        logging.error(
            f" while processing 'meta -> charset' tag of {file_path} -> error: {error_msg}"
        )

    try:
        meta_viewport_tag = html_file_soup.select('meta[name="viewport"]')
        if len(str(meta_viewport_tag)) < 5:
            html_file_soup.select("head")[0].insert(
                1,
                BeautifulSoup(
                    '<meta name="viewport" content="width=device-width,initial-scale=1">',
                    features="html.parser",
                ),
            )  # append correct document encoding/charset metadata tag
    except Exception as error_msg:
        logging.error(
            f" while processing 'meta -> viewport' tag of {file_path} -> error: {error_msg}"
        )

    # print(html_file_soup.select('head'))

    with open(file_path_str, "w+", encoding="utf-8") as html_file_new:
        html_file_new.write(str(html_file_soup).strip())


all_html_files = (
    list(working_dir.rglob("*.html"))
)
# pprint(all_html_files)
logging.debug(f'found "{len(all_html_files)}" html file(s)')

with concurrent.futures.ThreadPoolExecutor() as executor:  # docs see: https://docs.python.org/3/library/concurrent.futures.html#concurrent.futures.ThreadPoolExecutor    # max_workers=os.cpu_count()-1
    for html_file in all_html_files:
        try:
            future = executor.submit(cleanup_html_file, html_file)
            # returnVal = future.result()
            # print(returnVal)
        except Exception as error_msg:
            logging.error(
                f"failed to cleanup {html_file} -> error: {error_msg}"
            )

# ---------------------------------------------------------- #

last_mod_sitemap = ""
try:
    last_mod_sitemap = (
        "<lastmod>" + str(datetime.now(tzutc()).strftime("%Y-%m-%d")) + "</lastmod>"
    )
except Exception as error_msg:
    logging.error(f"failed to generate meta tag 'pubdate' -> error: {error_msg}")

#region sitemap.xml

sitemap_xml = '<?xml version="1.0" encoding="UTF-8"?>\n<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'
for file_path in all_html_files:
    sitemap_xml += (
        f"""
    <url>
    <loc>https://tinyweatherforecastgermanygroup.gitlab.io/twfg-javadoc/{str(file_path).replace('public/','').replace('../','')}</loc>
    """
        + last_mod_sitemap
        + """
    <changefreq>weekly</changefreq>
    </url>
    """
    )
sitemap_xml += "\n</urlset>"

with open(working_dir / "sitemap.xml", "w+", encoding="utf-8") as fh:
    fh.write(str(sitemap_xml))

#endregion

#region robots.txt -> for search engine bots

robotsTXT = """
User-agent: *
Allow: /

Sitemap: https://tinyweatherforecastgermanygroup.gitlab.io/twfg-javadoc/sitemap.xml
"""

with open(working_dir / "robots.txt", "w+", encoding="utf-8") as fh:
    fh.write(str(robotsTXT))

#endregion

#region HTML error page (404)

error_page404 = '<html lang="en"><head><title>Error 404</title><link rel="stylesheet" type="text/css" href="javadoc-styles.min.css" ></head><body><div>Page <strong>not</strong> found!</div></body></html>'
with open(str(Path(working_dir / "404.html").absolute()), "w+", encoding="utf-8") as fh:
    fh.write(str(error_page404))

#endregion

#region HTML error page (403)

error_page403 = '<html lang="en"><head><title>Error 403</title><link rel="stylesheet" type="text/css" href="javadoc-styles.min.css" ></head><body><div>Access <strong>denied</strong>!</div></body></html>'
with open(str(Path(working_dir / "403.html").absolute()), "w+", encoding="utf-8") as fh:
    fh.write(str(error_page403))

#endregion
