import concurrent.futures
import logging
from pathlib import Path

import rcssmin
from jsmin import jsmin

working_dir = Path("../public")
working_dir.mkdir(parents=True, exist_ok=True)  # create directory if not exists

log_p = working_dir / "debug.log"
try:
    logging.basicConfig(
        format="%(asctime)-s %(levelname)s [%(name)s]: %(message)s",
        level=logging.DEBUG,
        handlers=[
            logging.FileHandler(
                str(log_p.absolute()), encoding="utf-8"
            ),
            logging.StreamHandler(),
        ],
    )
except Exception as error_msg:
    logging.error(f"while logger init! -> error: {error_msg}")


def minify_js_file(file_path):
    """
    minify contents of js file file_path and
    delete the file afterwards
    """
    file_path_str = str(file_path.absolute())
    with open(file_path_str, "r", encoding="utf-8") as js_file_old:
        js_minified_str = str(jsmin(str(js_file_old.read())))

    file_path_min = file_path_str.replace(".js", ".min.js")

    with open(file_path_min, "w", encoding="utf-8") as js_file_new:
        js_file_new.write(js_minified_str)

    file_path.unlink()  # delete original file


def minify_css_file(file_path):
    """
    minify contents of css file file_path and
    delete the file afterwards
    """
    file_path_str = str(file_path.absolute())
    with open(file_path_str, "r", encoding="utf-8") as css_file_old:
        css_minified_str = str(rcssmin.cssmin(str(css_file_old.read())))

    file_path_min = file_path_str.replace(".css", ".min.css")

    with open(file_path_min, "w", encoding="utf-8") as css_file_new:
        css_file_new.write(css_minified_str)

    file_path.unlink()  # delete original file


all_js_files = (
    list(working_dir.rglob("*.js"))
)
# pprint(all_js_files)
logging.debug(f'found "{len(all_js_files)}" js file(s)')

# docs see: https://docs.python.org/3/library/concurrent.futures.html#concurrent.futures.ThreadPoolExecutor
# might control workers with 'max_workers=os.cpu_count()-1' in the future
with concurrent.futures.ThreadPoolExecutor() as executor:
    for js_file in all_js_files:
        try:
            if "script.js" in str(js_file):
                custom_script_js = """
var moduleSearchIndex;
var packageSearchIndex;
var typeSearchIndex;
var memberSearchIndex;
var tagSearchIndex;
//var pathtoroot = '';

function loadScripts(doc, tag) {
    createElem(doc, tag, 'jquery/jszip/dist/jszip.min.js');
    createElem(doc, tag, 'jquery/jszip-utils/dist/jszip-utils.min.js');
    if (window.navigator.userAgent.indexOf('MSIE ') > 0 || window.navigator.userAgent.indexOf('Trident/') > 0 || window.navigator.userAgent.indexOf('Edge/') > 0) {
        createElem(doc, tag, 'jquery/jszip-utils/dist/jszip-utils-ie.min.js');
    }
    createElem(doc, tag, 'search.min.js');
    $.get(pathtoroot + "package-search-index.zip").done(function() {
        JSZipUtils.getBinaryContent(pathtoroot + "package-search-index.zip", function(e, data) {
            JSZip.loadAsync(data).then(function(zip) {
                zip.file("package-search-index.json").async("text").then(function(content) {
                    packageSearchIndex = JSON.parse(content);
                });
            });
        });
    });
    $.get(pathtoroot + "type-search-index.zip").done(function() {
        JSZipUtils.getBinaryContent(pathtoroot + "type-search-index.zip", function(e, data) {
            JSZip.loadAsync(data).then(function(zip) {
                zip.file("type-search-index.json").async("text").then(function(content) {
                    typeSearchIndex = JSON.parse(content);
                });
            });
        });
    });
    $.get(pathtoroot + "member-search-index.zip").done(function() {
        JSZipUtils.getBinaryContent(pathtoroot + "member-search-index.zip", function(e, data) {
            JSZip.loadAsync(data).then(function(zip) {
                zip.file("member-search-index.json").async("text").then(function(content) {
                    memberSearchIndex = JSON.parse(content);
                });
            });
        });
    });
    if (!packageSearchIndex) {
        createElem(doc, tag, 'package-search-index.min.js');
    }
    if (!typeSearchIndex) {
        createElem(doc, tag, 'type-search-index.min.js');
    }
    if (!memberSearchIndex) {
        createElem(doc, tag, 'member-search-index.min.js');
    }
    /*$(window).resize(function() {
        $('.navPadding').css('padding-top', $('.fixedNav').css("height"));
    });*/
}

function createElem(doc, tag, path) {
    var script = doc.createElement(tag);
    var scriptElement = doc.getElementsByTagName(tag)[0];
    script.src = pathtoroot + path;
    scriptElement.parentNode.insertBefore(script, scriptElement);
}

function show(type) {
    count = 0;
    for (var key in data) {
        var row = document.getElementById(key);
        if ((data[key] & type) !== 0) {
            row.style.display = '';
            row.className = (count++ % 2) ? rowColor : altColor;
        } else
            row.style.display = 'none';
    }
    updateTabs(type);
}

function updateTabs(type) {
    for (var value in tabs) {
        var sNode = document.getElementById(tabs[value][0]);
        var spanNode = sNode.firstChild;
        if (value == type) {
            sNode.className = activeTableTab;
            spanNode.innerHTML = tabs[value][1];
        } else {
            sNode.className = tableTab;
            spanNode.innerHTML = '<a href=\"javascript:show(' + value + ');\">' + tabs[value][1] + '</a>';
        }
    }
}

function updateModuleFrame(pFrame, cFrame) {
    top.packageFrame.location = pFrame;
    top.classFrame.location = cFrame;
}

/*document.addEventListener('DOMContentLoaded', function (params) {
    loadScripts(document, 'script');
});*/
                """
                custom_script_js = str(
                    jsmin(str(custom_script_js))
                )

                with open(
                    str(js_file).replace(".js", ".min.js"), "w", encoding="utf-8"
                ) as script_js_file_new:
                    script_js_file_new.write(custom_script_js)

                js_file.unlink()

            elif ".min" not in str(js_file):
                future = executor.submit(minify_js_file, js_file)
                # returnVal = future.result()
                # print(returnVal)
            else:
                logging.debug(f"ignored '{js_file}' -> already minified")
        except Exception as error_msg:
            logging.error(f"failed to minify '{js_file}' -> error: {error_msg}")

all_css_files = (
    list(working_dir.rglob("*.css"))
)
# pprint(all_css_files)
logging.debug(f'found "{len(all_css_files)}" css file(s)')

with concurrent.futures.ThreadPoolExecutor() as executor:  # docs see: https://docs.python.org/3/library/concurrent.futures.html#concurrent.futures.ThreadPoolExecutor  # max_workers=os.cpu_count()-1
    for css_file in all_css_files:
        try:
            if ".min" not in str(css_file):
                future = executor.submit(minify_css_file, css_file)
                # returnVal = future.result()
                # print(returnVal)
            else:
                logging.debug(f"ignored '{css_file}' -> already minified")
        except Exception as error_msg:
            logging.error(
                f"failed to minify '{css_file}' -> error: {error_msg}"
            )
