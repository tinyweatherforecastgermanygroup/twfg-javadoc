"""
remove obsolete css rules from stylesheets
"""

import logging
import re
from pathlib import Path

import cssutils
from selectolax.parser import HTMLParser

public_p = Path("../public")

log_p = public_p / "debug.log"
try:
    logging.basicConfig(
        format="%(asctime)-s %(levelname)s [%(name)s]: %(message)s",
        level=logging.DEBUG,
        handlers=[
            logging.FileHandler(
                str(log_p.absolute()), encoding="utf-8"
            ),
            logging.StreamHandler(),
        ],
    )
except Exception as error_msg:
    logging.error(f"while logger init! -> error: {error_msg}")

css_files = list(public_p.rglob("*.css"))
js_files = list(public_p.rglob("*.js"))
html_files = list(public_p.rglob("*.html"))

logging.debug(f"found {len(css_files)} css files")
logging.debug(f"found {len(js_files)} js files")
logging.debug(f"found {len(html_files)} html files")

html_contents = []

for html_file in html_files:
    # logging.debug(f"processing '{html_file}'")

    with open(html_file, "r", encoding="utf-8") as file_handle:
        html = file_handle.read()

    if "body" not in html:
        continue

    # logging.debug(f"parsing '{html_file}' ...")
    tree = HTMLParser(html)

    html_contents.append({"path": html_file, "contents": html, "tree": tree})


js_contents = []
for js_file in js_files:
    # logging.debug(f"processing '{js_file}' ...")

    with open(js_file, "r", encoding="utf-8") as file_handle:
        js = file_handle.read()

    js_contents.append({"path": js_file, "contents": js})

    # break

total_rules = 0
obsolete_rules = 0

lb_pattern = re.compile(r"[\r\n\t]+")
ws_pattern = re.compile(r"( )+\s+")
for css_file in css_files:
    logging.debug(f"processing '{css_file}' ...")

    with open(css_file, "r", encoding="utf-8") as file_handle:
        css = file_handle.read()

    sheet = cssutils.parseString(css, validate=False, encoding="utf-8")

    for rule in sheet:
        total_rules += 1
        if rule.type == rule.STYLE_RULE:
            r_selector = str(rule.selectorText)

            if len(r_selector.replace("None", "").replace("*", "").strip()) == 0:
                continue

            if ", " in r_selector:
                r_selector = r_selector.split(", ")
            else:
                r_selector = [r_selector]

            r_found = False
            for r_selector_str in r_selector:
                if ":" in r_selector_str:
                    r_selector_str = r_selector_str.split(":", maxsplit=1)[0]

                for html_dict in html_contents:
                    r_matches = list(html_dict["tree"].css(r_selector_str))

                    if len(r_matches) > 0:
                        # logging.debug(f"found '{r_selector}' in '{html_dict['path']}'")
                        r_found = True
                        break

                if not r_found:
                    for js_dict in js_contents:
                        r_selector_tmp = r_selector_str
                        if " " in r_selector_tmp:
                            r_selector_tmp = r_selector_tmp.split(" ")[0]
                        r_selector_tmp = r_selector_str.strip(".").strip("#")

                        if r_selector_tmp in js_dict["contents"]:
                            r_found = True
                            break

                if not r_found:
                    logging.debug(f"failed to find '{r_selector_str}' in html and js files")
                else:
                    break

            if not r_found:
                obsolete_rules += 1
                del rule

            # break

    cssutils.ser.prefs.keepAllProperties = True
    cssutils.ser.prefs.keepUnknownAtRules = True

    css_text = sheet.cssText.decode("utf-8")
    css_text = re.sub(lb_pattern, "", css_text)
    css_text = re.sub(ws_pattern, " ", css_text)

    with open(css_file, "w", encoding="utf-8") as file_handle:
        file_handle.write(css_text)

logging.info(f"removed {obsolete_rules} of {total_rules} css rules")
